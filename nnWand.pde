  /*

 Charles Fried - 2017
 ANN Tutorial 
 Part #1
 
 MAIN TAB
 
 Original from Alasdair Turner (c) 2009
 Free software: you can redistribute this program and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 */
import java.io.FilenameFilter;
PImage img;
static final FilenameFilter FILTER = new FilenameFilter() {
  static final String NAME = "step", EXT = ".txt";
 
  @ Override boolean accept(File path, String name) {
    return name.startsWith(NAME) && name.endsWith(EXT);
  }
};
File f = dataFile("folderName");
String[] names = f.list(FILTER);

int training_wand_length = 50;
int index = 0;
int label = 0;
int totalTrain = 0;
int totalTest = 0;
int totalRight = 0;
float sucess = 0;
int testCard = 0;
int trainCard = 0;
float step = 0.1;
boolean onRealTime = false;
int number_hidden_layer = 64;
Network neuralnet;
Button trainB, testB,loadB,detecWand,saveWeight,trainWand,nextWand,backWand,testRealTime, testDataset;

reStore reStoreHidenLayer;
reStore reStoreOutputLayer;

Store StoreHidenLayer;
Store StoreOutputLayer;
void setup() {
  img = loadImage("logo.png");
  testByte();
  size(1000, 400);
  setupSigmoid();
  loadData();
  neuralnet = new Network(196, number_hidden_layer, 10);
  reStoreHidenLayer = new reStore("HidenLayer",number_hidden_layer,196);
  reStoreOutputLayer = new reStore("OutPutLayer",10,number_hidden_layer);
  StoreHidenLayer = new Store("HidenLayer",number_hidden_layer,196);
  StoreOutputLayer = new Store("OutPutLayer",10,number_hidden_layer);
  //load weights
  
  smooth();
  stroke(150);
  
  trainB = new Button(width*0.06, height*0.9, "Train");
  testB = new Button(width*0.11, height*0.9, "Test");
  loadB = new Button(width*0.16, height*0.9, "Load");
  saveWeight = new Button(width*0.21, height*0.9, "Save");
    nextWand = new Button(width*0.36, height*0.9, "Next");
  detecWand = new Button(width*0.31, height*0.9, "Wand");
    backWand = new Button(width*0.26, height*0.9, "Back");
  trainWand = new Button(width*0.41, height*0.9, "Train\nWand");
testRealTime = new Button(width*0.46, height*0.9, "Real\nTime");
testDataset = new Button(width*0.51, height*0.9, "Test\nAll");
  
}

void draw() {

  background(255);
  pushMatrix();
  image(img, width*0.93,height*0.85, 50, 50);
  text("ĐH BÁCH KHOA TP.HCM", width*0.77, height*0.92);
  text("Le Duc Phuong - 1413010 ", width*0.77, height*0.96);
  popMatrix();
  neuralnet.display();
  fill(100);
  pushMatrix();
  translate(-width*0.1, -0.135*height);
  text("Counter:" + index, width*0.50, height*0.85);
  text("Test card: #" + testCard, width*0.50, height*0.89);
  text("Train card: " + trainCard, width*0.50, height*0.93);
  
  text("Total train: " + totalTrain, width*0.61, height*0.89);
  text("Total test: " + totalTest, width*0.61, height*0.93);
  
  if(totalTest>0) sucess = float(totalRight)/float(totalTest);
  text("Success rate: " + nfc(sucess, 2), width*0.72, height*0.89);
  text("Card label: " + testing_set[testCard].output, width*0.72, height*0.93);
  
  popMatrix();
  trainB.display();
  testB.display();
  loadB.display();
  detecWand.display();
  saveWeight.display();
  nextWand.display();
  backWand.display();
  trainWand.display();
  testRealTime.display();
  testDataset.display();
  if(onRealTime){
  if(step > 1){
    step=0.1;
   label = 10;
   loadWand(label,0);
   neuralnet.respond(ImageWand[0]);
   neuralnet.display();
    }
    step += 0.1;
  }
}

void mousePressed() {
  if (trainB.hover()) {
    for (int i = 0; i < 100000; i++) {
      trainCard = (int) floor(random(0, training_set.length));
      neuralnet.respond(training_set[trainCard]);
      neuralnet.train(training_set[trainCard].outputs);
      totalTrain++;
    }
  } else if (testB.hover()){
    for (int i = 0; i < 1000; i++) {
    testCard = (int) floor(random(0, testing_set.length));
    neuralnet.respond(testing_set[testCard]);
    //load weights
    neuralnet.display();
    if(neuralnet.bestIndex == testing_set[testCard].output) totalRight ++;
    //println(testCard + "  " + neuralnet.bestIndex + "  " + testing_set[testCard].output);
    totalTest ++;
    }
  } else if(loadB.hover()){
  reStoreHidenLayer.load(neuralnet.hidden_layer);
  reStoreOutputLayer.load(neuralnet.output_layer);
  } else if(nextWand.hover()){
    //loadWand(index);
    //neuralnet.respond(ImageWand[0]);
    //neuralnet.display();
    index++;
    loadWand(label,index);
     neuralnet.respond(ImageWand[0]);
     neuralnet.display();
  } else if(backWand.hover()) {
    index--;
    loadWand(label,index);  
    neuralnet.respond(ImageWand[0]);
    neuralnet.display();
  } else 
  if(saveWeight.hover()){
    StoreHidenLayer.save(neuralnet.hidden_layer);
    StoreOutputLayer.save(neuralnet.output_layer);
    print("Saved");
  } else if(trainWand.hover()){
      for (int i = 0; i <100000; i++) {
     index = (int) floor(random(0, training_wand_length));
     label = (int) floor(random(0,10));
    loadWand(label,index);
    neuralnet.respond(ImageWand[0]);
    neuralnet.train(ImageWand[0].outputs);
      }
    //index++;
    
  } else if(detecWand.hover()){
   index = (int) floor(random(0, training_wand_length));
   label = (int) floor(random(0, 10));
   loadWand(label,index);
   neuralnet.respond(ImageWand[0]);
   neuralnet.display();
  } else if(testRealTime.hover()) {
    onRealTime = !onRealTime;
  } else if(testDataset.hover()){
  for(int l = 0 ; l < 10; l ++){
  for(int i = training_wand_length ; i < 60 ; i++){
  loadWand(l,i);
  neuralnet.respond(ImageWand[0]);
  neuralnet.display();
  if(neuralnet.bestIndex == l) totalRight ++;
  totalTest ++;
  }
  }
  
  }
    
  
  redraw();
}
