/*

 Charles Fried - 2017
 ANN Tutorial 
 Part #2
 
 NEURON
 
 This class is for the neural network, which is hard coded with three layers: input, hidden and output
 
 */
 
float LEARNING_RATE = 0.01;


class Neuron {

  Neuron [] inputs; // Strores the neurons from the previous layer
  float [] weights;
  float output;
  float error;

  Neuron() {
    error = 0.0;
  }

  Neuron(Neuron [] p_inputs) {
    //add input, need add weights
    inputs = new Neuron [p_inputs.length];
    weights = new float [p_inputs.length];
    error = 0.0;
    for (int i = 0; i < inputs.length; i++) {
      inputs[i] = p_inputs[i];
      weights[i] = random(-1.0, 1.0);
    }
  }
  
  void respond(Neuron [] p_inputs) {
//

    float input = 0.0;
    for (int i = 0; i < inputs.length; i++) {
      input += p_inputs[i].output * weights[i];///tat ca output layre troc nhan trong so ra dc 1 input cua neuron sau
    }
    output = lookupSigmoid(input); //activfuncton
    //output = lookupReLU(input); //activfuncton
    error = 0.0;
  }
  
   void setError(float desired) {
    error = desired - output;
  }
  
  void train() {
    //float delta = 0;
    //if(output <=0) {delta = 0; } else {
    //  delta = output*error * LEARNING_RATE;
    //}
    float delta =(1.0 - output) * (1.0 + output) * //daoham cua sigmod
      error * LEARNING_RATE; // chu y da nhan erro 
    for (int i = 0; i < inputs.length; i++) {
      inputs[i].error += weights[i] * error;///tong ca erru 1 neruon voi tanbo neuron dang sau
      weights[i] += inputs[i].output * delta; //cap nhatrong so bang SG x=x_+delta
    }
  }

  void display() {
    stroke(200);
    fill(128 * (1 - output));
    ellipse(0, 0, 16, 16);
    
    //textSize(10);
    //text("word", 12, 45, -30);  // Specify a z-axis value
  }
    void display(int i) {
    stroke(200);
    fill(128 * (1 - output));
    ellipse(0, 0, 16, 16);
    
    textSize(10);
    fill(0, 102, 153, 204);
    text(i,0, 0);  // Specify a z-axis value
  }
  void getWeights(){
    for(int i =0;i <inputs.length ;i++){
     print("w["+i+"]="+weights[i]) ;
    }
    println("");
  }
}
